import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { UpdateTodoComponent } from './components/update-todo/update-todo.component';

const routes: Routes = [
  { path: 'Home', component: HomeComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'ColorPicker', component: ColorPickerComponent },
  { path: 'TodoList', component: TodoListComponent },
  { path: 'ObservableDemo', component: ObservableDemoComponent },
  { path: 'AddTodo', component: AddTodoComponent },
  { path: 'UpdateTodo/:id', component: UpdateTodoComponent },
  { path: '**', redirectTo: 'TodoList' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
