import { Component, Input, OnInit } from '@angular/core';
import { MyColor } from 'src/app/models/color';
import * as _ from 'lodash';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

  public backgroundColor: string;

  public colors : MyColor[];

  @Input() public r?: number;
  @Input() public g?: number;
  @Input() public b?: number;
  @Input() public a?: number;

  constructor() { }

  ngOnInit(): void {
    
    // Test if r is null
    if (this.r == null) {
      this.r = Math.random() * 255;
    }
    if (this.g == null) {
      this.g = Math.random() * 255;
    }

    if (this.b == null) {
      this.b = Math.random() * 255;
    }

    if (this.a == null) {
      this.a = Math.random();
    }

    this.updateColor();
  }

  public updateColor(): void {
    this.backgroundColor = `rgba(${this.r},${this.g},${this.b},${this.a})`;
  }
}
