import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';
import { WeatherService } from 'src/app/services/weather.service';
import { Injector } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  public todos$: Observable<Todo[]>;

  constructor(
    private todoListService: TodoService,
    private injector: Injector
  ) { }

  ngOnInit(): void {
    this.todos$ = this.todoListService.getTodos();

    console.log('TodoListComponent - End of ngOnInit');

    const weatherService: WeatherService = this.injector.get(WeatherService);
  }

  public updateTodo(todo: Todo): void {
    this.todoListService.createOrUpdateTodo(todo).subscribe();
  }

  public removeTodo(todo: Todo): void {
    this.todoListService.deleteTodo(todo.id).subscribe();
  }
}
