import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public routes: { label: string, path: string, hidden?: boolean }[];

  constructor() { }

  ngOnInit(): void {
    this.routes = [
      { label: 'Home', path: 'Home' },
      { label: 'Contact', path: 'Contact', hidden: true },
      { label: 'Color Picker', path: 'ColorPicker' },
      { label: 'Todo List', path: 'TodoList' },
      { label: 'Observable démo', path: 'ObservableDemo' }
    ]
  }
}
