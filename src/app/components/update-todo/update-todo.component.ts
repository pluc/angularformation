import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.scss']
})
export class UpdateTodoComponent implements OnInit {

  public todo$: Observable<Todo>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private todoService: TodoService) { }

  ngOnInit(): void {
    let returnToHome = false;
    let todoId;

    // Si on a de paramètre dans la route
    if (!this.activatedRoute.snapshot.params) {
      returnToHome = true;
    } else {
      todoId = this.activatedRoute.snapshot.params.id;
      returnToHome = todoId == null;
    }

    if (returnToHome) {
      this.router.navigate(['/']);
      return;
    }

    this.todo$ = this.todoService.getTodo(todoId);

  }

}
