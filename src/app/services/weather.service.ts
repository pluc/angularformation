import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { WeatherRoot } from '../models/weather';

@Injectable()
export class WeatherService {

    constructor(private httpClient: HttpClient) { }

    public getWeather(lat: number, lon: number): Observable<WeatherRoot> {
        const url: string = `https://fcc-weather-api.glitch.me/api/current?lat=${lat}&lon=${lon}`;

        console.log('A call to getWeather has been made');

        const result$: Observable<WeatherRoot> = this.httpClient.get<WeatherRoot>(url).pipe(map(res => {
            // On passe dans ce code là qu'après avoir fait un subscribe sur l'obserable retourné
            console.info('Weather received from api', res);
            return res;
        }));

        return result$;
    }
}
